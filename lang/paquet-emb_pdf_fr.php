<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'emb_pdf_description' => 'Ce plugin contient un modèle servant à afficher un PDF dans un article avec <code><embNNN|center></code>',
	'emb_pdf_nom' => 'Modèle PDF (visualiseur)',
	'emb_pdf_slogan' => 'Un modèle pour afficher un PDF dans un article',
);
