<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function emb_pdf_insert_head_css($flux){
	static $done = false;
	if (!$done) {
		$done = true;
		$flux .= '<link rel="stylesheet" href="'.find_in_path('css/emb_pdf.css').'" type="text/css" />';
	}
	return $flux;
}

function emb_pdf_insert_head($flux){
	$flux = emb_pdf_insert_head_css($flux); // au cas ou il n'est pas implemente
	return $flux;
}
